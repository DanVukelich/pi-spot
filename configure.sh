#!/bin/bash

if [[ ! -d "ansible/" ]] ; then
    echo "Error: Please run script from top level directory."
    exit 1
fi

if [[ -f "ansible/inventory.yaml" ]] ; then
    echo "Detected previous setup.  Overwrite and continue? (y/N)"
    read continue
    if [[ "${continue}" != [Yy]* ]]; then
	echo "Declined to overwrite.  Operation canceled."
	exit 1
    else
	rm "ansible/inventory.yaml"
    fi
fi


search_file=JS8Call.ini
echo "Searching for JS8Call config file.  Please wait..."
readarray -d '' configs < <(find ~ -name ${search_file} -print0 2>/dev/null)
if [[ ${#configs[@]} -eq 0 ]]; then
    echo "Could not find a JS8 config file on current host."
    echo "We expect a file called ${search_file} somewhere in current user's \$HOME"
    exit 1
elif [[ ${#configs[@]} -eq 1 ]]; then
    js8_config=${configs[0]}
else
    echo "Found multiple configs.  Type number of the one to use:"
    for (( i=0; i<${#configs[@]}; i++ ));
    do
	echo "[$i]: ${configs[$i]}"
    done
    read choice
    js8_config=${configs[$choice]}
fi

echo "Using ${js8_config}"


me=$(whoami)
echo "Username for ssh on Pi-Spot (leave blank for default '${me}'):"
read pispot_usr
if [[ -z "$pispot_usr" ]]; then
    pispot_usr=$me
fi

echo "IP/Hostname of the Pi-Spot:"
read pispot_addr

echo "Set password for the wireless network Pi-Spot will create:"
read hotspot_pw
if [[ "${#hotspot_pw}" -lt 8 ]]; then
    echo "Password too short.  Must be at least 8 characters."
    exit 1
fi

echo "Confirm Pi-Spot wifi password will be '${hotspot_pw}': (y/N)"
read continue
if [[ "${continue}" != [Yy]* ]]; then
    echo "Password declined.  Cancel operation."
    exit 1
fi


echo "pihost:
  hosts:
    ${pispot_addr}
  vars:
    hotspot_pw: ${hotspot_pw}
    ansible_user: ${pispot_usr}
    config_file: ${js8_config}
" > ansible/inventory.yaml

echo "Setup is complete.  You can now execute ./deploy.sh"
