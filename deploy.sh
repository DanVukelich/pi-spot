#!/bin/bash

if [[ ! -f "ansible/inventory.yaml" ]] ; then
    echo "Setup has not been run"
    echo "Run ./configure.sh first"
    exit 1
fi

ansible-playbook -i ansible/inventory.yaml ansible/playbook.yaml
