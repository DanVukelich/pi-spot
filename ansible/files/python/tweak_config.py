#!/usr/bin/env python3

from configparser import ConfigParser
from pathlib import Path
from sys import argv

override_settings = dict({
    'Configuration': dict({
        'AcceptTCPRequests': 'true',
        'TCPEnabled': 'true',
        'TCPMaxConnections': '1',
        'TCPServer': '127.0.0.1',
        'TCPServerPort': '2442'
        })
})

delete_settings = dict({
    'LogQSO': ['geometry'],
    'Configuration': ['WindowGeometry', 'AzElDir', 'SaveDir'],
    'MainWindow': ['geometry', 'geometryNoControls', 'MRUdir'],
    'WideGraph': ['geometry']
})

print(override_settings)

if __name__ == '__main__':
    config = ConfigParser();
    config.optionxform = str

    configPath = '/home/' + argv[1] + '/.config/JS8Call.ini'
    config.read(configPath)

    for section, settings in override_settings.items():
        for key, value in settings.items():
            config[section][key] = value

    for section, settings in delete_settings.items():
        for setting in settings:
            config.remove_option(section, setting)

    with open(configPath, 'w') as outFile:
        config.write(outFile, space_around_delimiters=False)
    
    
