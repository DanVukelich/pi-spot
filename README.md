# Pi-Spot
Licensed under GNU GPL v3

Configures a Raspberry Pi to act as a headless JS8Call host.  Send simple digitial-mode messages using your phone and the Pi-Spot web interface when out in the field.  Intended for light POTA/SOTA self-spotting.

During startup, Pi-Spot will create a wifi hotspot named `PiSpot`.  The raspberry pi will be your router with IP address 10.0.0.1, but you can also access it through the hostname `pi-spot.lan`.

Your main way of interfacing will be the Pi-Spot web UI, available at `http://pi-spot.lan`.

JS8Call settings are copied from your host machine.  If you need to change advanced settings, open a GUI session by connecting to the VNC server on port `5900` (eg `pi-spot.lan:5900`).

## Requirements
A raspberry pi set up with ssh.  It should be accessible through public key (password ssh not supported yet).  The ssh connection must be made through ethernet, as the setup process will configure a wifi hotspot on the raspberry pi's wlan interface.

I recommend using the raspberry pi imaging tool to set up a RaspiOS Lite image.  With Ctrl-Shift-X in the tool, you can embed your ssh public key in the image and enable ssh by default.

## Installation
First, configure your Pi-Spot installation by running the `configure.sh` script.  It will generate an inventory.yaml file with your settings.

The command `deploy.sh` will provision the raspberry pi to be a Pi-Spot.

## Usage
TBD